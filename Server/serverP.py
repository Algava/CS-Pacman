import zmq
import json
import sys
import os
import time

PORT = '50000'

def i2b(int_data):

    return int_data.to_bytes(8, 'big')


def b2i(bytes_data):

    return int.from_bytes(bytes_data, 'big')


def s2b(string_data):

    return string_data.encode('ascii')


def b2s(bytes_data):

    return bytes_data.decode('ascii')


class TransportLayer:


    def __init__(self):

        # Crea el contexto y el socket de tipo Router
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.ROUTER)

        # Lista que almacena las identidades que se han conectado
        self.identities = []

        self.bind(PORT)

        self.poller = zmq.Poller()

        self.poller.register(self.socket, zmq.POLLIN)


    def bind(self, port, ip = "*", protocol = "tcp"):
        '''
        Coloca a escuchar el socket por los parametros indicados
        '''
        self.socket.bind("{}://{}:{}".format(protocol, ip, port))
        print("   --- Started Server ---")


    def send(self, destination_ID, message):
        '''
        Envia un mensaje multiple por el socket hacia una identidad destino, 
        ambos pasados como parametros
        '''
        self.socket.send_multipart([destination_ID] + message)


    def multicast(self, message, identities_ids):
        '''
        Envia un mensaje multiple por el socket hacia una lista de identidades
        destino, ambos pasados como parametros
        '''
        for identity in identities_ids:
            self.send(identity, message)


    def broadcast(self, message):
        '''
        Envia un mensaje multiple por el socket hacia todos los destinatarios
        almacenados en la lista identities, el mensaje se pasa como parametro
        '''
        for identity in self.identities:
            self.send(identity, message)


    def recv(self):
        '''
        Retorna un mensaje multiple recibido en el socket
        '''
        return self.socket.recv_multipart()


    def add_identity(self, identity):
        '''
        Agrega una identidad pasada como parametro a la lista de identidades
        '''
        self.identities.append(identity)


    def delete_identity(self, identity):
        '''
        Elimina una identidad pasada como parametro de la lista de identidades
        '''
        self.identities.remove(identity)


    def find_identity(self, identity):
        '''
        Encuentra una identidad pasada como parametro en la lista de identidades
        '''
        return identity in self.identities


    def poll(self):
        '''
        Verifica si existe o no algun mensaje en la cola de recibidos del socket
        '''
        event = dict(self.poller.poll(0))
        if self.socket in event:
            return True
        else:
            return False


class NetworkingLayer(TransportLayer):


    def __init__(self):
        TransportLayer.__init__(self)
        self.last_requester = None


    def available_identity(self, identity):
        '''
        Verifica si una identidad esta disponible o no
        '''
        if not self.find_identity(identity):
            self.send(self.last_requester, [b'FREE_IDENTITY'])
        else:
            self.send(self.last_requester, [b'TAKEN_IDENTITY'])


    def fetch_data(self):
        '''
        Retorna los datos enviados por el cliente y guarda la identidad de quien 
        hizo la ultima solicitud en self.last_requester
        '''
        client_message = self.recv()
        self.last_requester = client_message[0]
        return client_message[1:]


    def new_client(self):
        '''
        Agrega la identidad del nuevo cliente a la lista de identidades
        '''
        self.add_identity(self.last_requester)


    def current_objects(self, objects_start, objects_dictionary):
        '''
        Envia todos los objectos actuales del juego, menos los que respecta al mapa
        '''
        for object_id in objects_dictionary:
            if object_id >= objects_start:

                object_creator = objects_dictionary[object_id]['OBJECT_CREATOR']
                object_group = objects_dictionary[object_id]['OBJECT_GROUP']
                object_owner = objects_dictionary[object_id]['OBJECT_OWNER']
                self.send(self.last_requester, [b'CREATE_OBJECT', i2b(object_id), s2b(object_creator), s2b(object_group), object_owner])


    def create_object(self, object_id, object_creator, object_group, object_owner):

        self.broadcast([b'CREATE_OBJECT', i2b(object_id), s2b(object_creator), s2b(object_group), object_owner])


    def update_object(self, object_id, object_parameters):

        self.broadcast([b'UPDATE_OBJECT', i2b(object_id), s2b(object_parameters)])


    def delete_object(self, object_id):

        self.broadcast([b'DELETE_OBJECT', i2b(object_id)])


    def change_group(self, object_id, new_group):

        self.broadcast([b'CHANGE_GROUP', i2b(object_id), s2b(new_group)])


    def game_state_player(self, player, new_state):

        self.send(player, [b'GAME_STATE_PLAYER', s2b(new_state)])


    def game_settings(self, game_settings):

        self.send(self.last_requester, [s2b(game_settings)])


class GameLayer:


    def __init__(self, map_json_file):
        self.objects = {}

        self.free_id = 0

        self.map_json_file = map_json_file


    def load_map_info(self):
        with open(self.map_json_file) as file:
            json_data = json.load(file)

        for layer in json_data['layers']:

            for index, tile_number in enumerate(layer['data']):

                if tile_number != 0:

                    px = (index % layer['width']) * json_data['tilewidth']
                    py = (index // layer['width']) * json_data['tileheight']

                    if tile_number == 34:

                        self.spawn_pos = [px, py]

                    elif tile_number == 245:
                        
                        self.score_start = [px, py]

                    self.get_free_id()

        self.objects_start = self.free_id + 1


    def create_object(self, object_id, object_creator, object_group, object_owner):

        self.objects[object_id] = {}
        self.objects[object_id]['OBJECT_CREATOR'] = object_creator
        self.objects[object_id]['OBJECT_GROUP'] = object_group
        self.objects[object_id]['OBJECT_OWNER'] = object_owner


    def get_free_id(self):
        self.free_id += 1
        return self.free_id


class PacmanServer:


    def __init__(self, map_json_file, w, h):
        self.networkingLayer = NetworkingLayer()
        self.gameLayer = GameLayer(map_json_file)

        self.game_settings = {}
        self.game_settings['map_file'] = map_json_file
        self.game_settings['display_w'] = w
        self.game_settings['display_h'] = h


    def requests(self):

        while self.networkingLayer.poll():

            request, *data = self.networkingLayer.fetch_data()

            if request == b'UPDATE_OBJECT':

                object_id = b2i(data[0])
                object_parameters = b2s(data[1])
                self.networkingLayer.update_object(object_id, object_parameters)               

            elif request == b'DELETE_OBJECT':

                object_id = b2i(data[0])
                self.networkingLayer.delete_object(object_id)

            elif request == b'CHANGE_GROUP':

                object_id = b2i(data[0])
                new_group = b2s(data[1])
                self.networkingLayer.change_group(object_id, new_group)

            elif request == b'GAME_STATE_PLAYER':

                object_owner = data[0]
                new_state = b2s(data[1])
                self.networkingLayer.game_state_player(object_owner, new_state)

            elif request == b'CREATE_OBJECT':

                object_id = self.gameLayer.get_free_id()
                object_creator = b2s(data[0])
                object_group = b2s(data[1])
                object_owner = self.networkingLayer.last_requester
                self.networkingLayer.create_object(object_id, object_creator, object_group, object_owner)
                self.gameLayer.create_object(object_id, object_creator, object_group, object_owner)

            elif request == b'CURRENT_OBJECTS':

                objects_dictionary = self.gameLayer.objects
                objects_start = self.gameLayer.objects_start
                self.networkingLayer.current_objects(objects_start, objects_dictionary)

            elif request == b'GAME_SETTINGS':

                self.game_settings['player_id'] = self.gameLayer.free_id + 1
                self.game_settings['score_start'][1] += 40
                game_settings = str(self.game_settings)
                self.networkingLayer.game_settings(game_settings)

            elif request == b'AVAILABLE_IDENTITY':

                identity = data[0]
                self.networkingLayer.available_identity(identity)

            elif request == b'NEW_CLIENT':

                self.networkingLayer.new_client()


    def run(self):
        
        os.system('clear')
        print("   --- PACMAN SERVER --- ")
        print(" STATUS --> Running...")

        w = self.game_settings['display_w']
        h = self.game_settings['display_h']

        self.gameLayer.load_map_info()

        self.game_settings['player_spawn'] = self.gameLayer.spawn_pos
        self.game_settings['score_start'] = self.gameLayer.score_start

        # Tiempo de espera para empezar
        timer = 30
        t1 = time.time()

        end_aux = False

        while True:
            self.requests()
            if ((timer - int(time.time() - t1)) == 0) and not end_aux:
                end_aux = True

                self.networkingLayer.broadcast([b'GAME_STATE_PLAYER', b'PLAYING'])


def main():
    print("   --- MAP SELECTION ---")
    print(" 1. Maze 15x15")
    #print(" 2. Maze 23x15")
    while True:
        
        sel = input(" Selection -- > ")
        if sel == '1':
            map_json_file = 'Maze/map18x15.json'
            w = 900
            h = 750
            break

        else:
            print(' Wrong selection, try again')

    pacmanServer = PacmanServer(map_json_file, w, h)

    try:
        pacmanServer.run()
    except KeyboardInterrupt:
        print(" STATUS --> Ended") 


if __name__ == "__main__":

    main()