import zmq
import sys
import pygame
import json
import os
import time

IP = 'localhost'
PORT = '50000'

def i2b(int_data):

    return int_data.to_bytes(8, 'big')


def b2i(bytes_data):

    return int.from_bytes(bytes_data, 'big')


def s2b(string_data):

    return string_data.encode('ascii')


def b2s(bytes_data):

    return bytes_data.decode('ascii')


class DefaultObject(pygame.sprite.Sprite):


    def __init__(self, surface, px, py):
        pygame.sprite.Sprite.__init__(self)
        self.image = surface
        self.rect = self.image.get_rect()
        self.rect.x = px
        self.rect.y = py
        self.mask = pygame.mask.from_surface(self.image)


class Wall(DefaultObject):


    def __init__(self, surface, px, py):

        DefaultObject.__init__(self, surface, px, py)


class Cookie(DefaultObject):


    def __init__(self, surface, px, py):

        DefaultObject.__init__(self, surface, px, py)


class Fruit(DefaultObject):


    def __init__(self, surface, px, py):

        DefaultObject.__init__(self, surface, px, py)


class Player(pygame.sprite.Sprite):


    def __init__(self, tile_json_file, x, y, fps):

        pygame.sprite.Sprite.__init__(self)
        self.sprite_number = 0
        self.kind = 'PACMAN'
        self.action = 'STOP'
        self.direction = 'RIGHT'
        self.load_sprites(tile_json_file)
        self.image = self.sprites[self.kind][self.action][self.direction][self.sprite_number]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.pos = [x, y]
        self.score = 0
        
        self.change_sprite = False
        self.change_factor = fps // 15


    def update(self):

        if self.change_sprite: 
            self.sprite_number = 0
            self.change_sprite = False        

        if (self.sprite_number // self.change_factor) == len(self.sprites[self.kind][self.action][self.direction]):
            self.sprite_number = 0

        self.image = self.sprites[self.kind][self.action][self.direction][self.sprite_number // self.change_factor]
        self.sprite_number += 1

        self.rect.x = self.pos[0]
        self.rect.y = self.pos[1]


    def load_sprites(self, tile_json_file):

        self.sprites = {}
        tile_set = TileSet(tile_json_file)
        self.sprites['PACMAN'] = {}

        self.sprites['PACMAN']['MOVE']= {}
        self.sprites['PACMAN']['MOVE']['RIGHT'] = tile_set.get_tile_list([[17, 0], [17, 1], [17, 2]])
        self.sprites['PACMAN']['MOVE']['DOWN'] = tile_set.get_tile_list([[17, 3], [17, 5], [17, 4]])
        self.sprites['PACMAN']['MOVE']['LEFT'] = tile_set.get_tile_list([[17, 6], [17, 7], [17, 8]])
        self.sprites['PACMAN']['MOVE']['UP'] = tile_set.get_tile_list([[17, 9], [17, 10], [17, 11]])

        self.sprites['PACMAN']['STOP'] = {}
        self.sprites['PACMAN']['STOP']['RIGHT'] = tile_set.get_tile_list([[17, 1]])
        self.sprites['PACMAN']['STOP']['DOWN'] = tile_set.get_tile_list([[17, 5]])
        self.sprites['PACMAN']['STOP']['LEFT'] = tile_set.get_tile_list([[17, 7]])
        self.sprites['PACMAN']['STOP']['UP'] = tile_set.get_tile_list([[17, 10]])

        self.sprites['MONSTER'] = {}

        self.sprites['MONSTER']['MOVE'] = {}
        self.sprites['MONSTER']['MOVE']['RIGHT'] = tile_set.get_tile_list([[0, 11], [0, 12]])
        self.sprites['MONSTER']['MOVE']['DOWN'] = tile_set.get_tile_list([[0, 13], [0, 14]])
        self.sprites['MONSTER']['MOVE']['LEFT'] = tile_set.get_tile_list([[0, 15], [0, 16]])
        self.sprites['MONSTER']['MOVE']['UP'] = tile_set.get_tile_list([[0, 17], [0, 18]])

        self.sprites['MONSTER']['STOP'] = {}
        self.sprites['MONSTER']['STOP']['RIGHT'] = tile_set.get_tile_list([[0, 12]])
        self.sprites['MONSTER']['STOP']['DOWN'] = tile_set.get_tile_list([[0, 14]])
        self.sprites['MONSTER']['STOP']['LEFT'] = tile_set.get_tile_list([[0, 16]])
        self.sprites['MONSTER']['STOP']['UP'] = tile_set.get_tile_list([[0, 18]])


class PlayerTitle(pygame.sprite.Sprite):


    def __init__(self, font_file, font_size, player_id, name, color, objects):
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(font_file, font_size)
        self.player = objects[player_id]['OBJECT_DATA']
        self.name = name
        self.color = color


    def render(self, x, y):
        size = self.font.size(self.name)
        x = x - size[0]//2
        y = y - size[1]
        self.image = self.font.render(self.name, True, self.color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y 


    def update(self):
        pos = self.player.rect.midtop
        self.render(pos[0], pos[1])


class PlayerScore(pygame.sprite.Sprite):


    def __init__(self, font_file, font_size, player_id, pos, name, color, objects):
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(font_file, font_size)
        self.player = objects[player_id]['OBJECT_DATA']
        self.name = name
        self.color = color
        self.pos = pos


    def render(self, x, y):
        score = '{0}{1: >10}'.format(self.name, self.player.score)
        size = self.font.size(score)
        x = x + 25 - size[0]//2
        y = y
        self.image = self.font.render(score, True, self.color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y 


    def update(self):

        self.render(self.pos[0], self.pos[1])


class Text(pygame.sprite.Sprite):


    def __init__(self, font_file, font_size, color, pos, data):
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(font_file, font_size)
        self.color = color
        self.pos = pos
        self.data = data


    def render(self, x, y):
        size = self.font.size(self.data)
        x = x - size[0]//2
        y = y
        self.image = self.font.render(self.data, True, self.color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y 


    def update(self):

        self.render(self.pos[0], self.pos[1])


class TransportLayer:


    def __init__(self):

        # Crea el contexto y el socket de tipo Dealer
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.DEALER)

        # Crea al poller para que registre si existe o no actividad en el socket
        self.poller = zmq.Poller()

        #self.poller.register(sys.stdin, zmq.POLLIN)
        self.poller.register(self.socket, zmq.POLLIN)


    def connect(self, port, ip, protocol):
        '''
        Realiza la conexion del socket con los parametros dados
        '''
        self.socket.connect("{}://{}:{}".format(protocol, ip, port))


    def send(self, message):
        '''
        Envia un mensaje multiple por el socket, pasado como parametro
        '''
        self.socket.send_multipart(message)


    def recv(self):
        '''
        Retorna un mensaje multiple recibido en el socket
        '''
        return self.socket.recv_multipart()


    def poll(self, waiting):
        '''
        Verifica si existe o no algun mensaje en la cola de recibidos del socket
        '''
        event = dict(self.poller.poll(waiting))
        if self.socket in event:
            return True
        else:
            return False


    def available_identity(self, identity):
        '''
        Verifica disponibilidad de la identidad pasada como parametro
        '''
        context = zmq.Context()
        socket = self.context.socket(zmq.DEALER)
        socket.connect("tcp://{}:{}".format(IP, PORT))
        socket.send_multipart([b'AVAILABLE_IDENTITY', identity.encode('ascii')])
        response = socket.recv()
        socket.close()
        context.term()
        if response == b'FREE_IDENTITY':
            return True
        else:
            return False 


    def set_identity(self, identity):
        '''
        Asigna la identidad del socket, pasada como parametro
        '''
        self.socket.identity = identity


class NetworkingLayer(TransportLayer):


    def __init__(self):

        TransportLayer.__init__(self)


    def new_client(self):
        '''
        DONE
        Solicitud para dar aviso al servidor de que es un nuevo cliente
        '''
        self.connect(PORT, IP, 'tcp')
        self.send([b'NEW_CLIENT'])


    def current_objects(self):
        '''
        DONE
        Solicitud para obtener todos los objetos actuales del juego
        '''
        self.send([b'CURRENT_OBJECTS'])


    def create_object(self, object_creator, object_group):
        '''
        DONE
        Solicitud para crear un objeto en el juego de acuerdo a las caracteristicas
        del objeto ingresadas por parametro y a cual grupo de objetos debe pertenecer
        '''
        self.send([b'CREATE_OBJECT', s2b(object_creator), s2b(object_group)])


    def delete_object(self, object_id):
        '''
        DONE
        Solicitud para eliminar un objeto del juego
        '''
        self.send([b'DELETE_OBJECT', i2b(object_id)])


    def update_object(self, object_id, object_parameters):
        '''
        DONE
        Solicitud para actualizar los parametros de un objeto en el juego
        '''
        self.send([b'UPDATE_OBJECT', i2b(object_id), s2b(object_parameters)])


    def change_group(self, object_id, new_group):
        '''
        '''
        self.send([b'CHANGE_GROUP', i2b(object_id), s2b(new_group)])


    def game_state_player(self, object_owner, new_state):
        '''
        Actualizar el estado de juego de un jugador
        '''
        self.send([b'GAME_STATE_PLAYER', object_owner, s2b(new_state)])


    def fetch_data(self):
        '''
        Retorna lo que se debe crear, eliminar o actualizar y sus respectivos datos
        '''
        return self.recv()


    def game_settings(self):

        self.send([b'GAME_SETTINGS'])
        return eval(b2s(self.recv()[0]))


class TileSet:


    def __init__(self, tileset_json_file):
        self.json_data = self.json_data(tileset_json_file)
        self.tile_width = self.json_data['tilewidth']
        self.tile_height = self.json_data['tileheight']
        self.image_file = self.json_data['image']
        self.image_width = self.json_data['imagewidth']
        self.image_height = self.json_data['imageheight']
        self.image = pygame.image.load('Maze/{}'.format(self.image_file))
        self.crop()


    def json_data(self, tile_set_json):
        with open(tile_set_json) as file:
            return json.load(file)


    def crop(self):
        self.tile_list = []

        self.tile_matrix = []
        for y in range(0, self.image_height, self.tile_height):

            x_matrix = []
            for x in range(0, self.image_width, self.tile_width):
                tile_surface = self.image.subsurface(x, y, self.tile_width, self.tile_height)
                try:
                    tile_type = self.json_data['tileproperties'][str(len(self.tile_list))]
                except:
                    tile_type = {'object':'None'}

                x_matrix.append([tile_surface, tile_type]) 

                self.tile_list.append([tile_surface, tile_type])

            self.tile_matrix.append(x_matrix)


    def get_tile(self, point):
        tile = self.tile_matrix[point[1]][point[0]][0]
        return tile


    def get_tile_list(self, point_list):
        tile_list = []
        for point in point_list:
            tile = self.get_tile(point)
            tile_list.append(tile)

        return tile_list


class TileMap:


    def __init__(self, tilemap_json_file, groups):
        self.json_data = self.json_data(tilemap_json_file)
        self.tile_sets = {}
        self.tile_list = []
        self.load_tile_sets()
        self.groups = groups


    def json_data(self, tilemap_json_file):
        with open(tilemap_json_file) as file:
            return json.load(file)


    def load_tile_sets(self):
        
        for tile_set_data in self.json_data['tilesets']:
            tile_set = TileSet('Maze/{}'.format(tile_set_data['source']))
            self.tile_sets[tile_set.json_data['name']] = tile_set
            self.tile_list += tile_set.tile_list


class GameLayer:


    def __init__(self):
        self.objects = {}

        self.groups = {}
        self.groups['PLAYERS'] = pygame.sprite.Group()
        self.groups['TITLES'] = pygame.sprite.Group()
        self.groups['ENEMIES'] = pygame.sprite.Group()
        self.groups['WALLS'] = pygame.sprite.Group()
        self.groups['COOKIES'] = pygame.sprite.Group()
        self.groups['FRUITS'] = pygame.sprite.Group()
        self.groups['ALL'] = pygame.sprite.Group()

        self.free_id = 0


    def find_object_id(self, object_data):
        for object_id in self.objects:
            if self.objects[object_id]['OBJECT_DATA'] == object_data:
                return object_id


    def get_free_id(self):
        self.free_id += 1
        return self.free_id


    def load_map(self, tile_map):

        json_map = tile_map.json_data

        for layer in json_map['layers']:

            for index, tile_number in enumerate(layer['data']):

                if tile_number != 0:

                    tile = tile_map.tile_list[tile_number - 1][0]
                    tile_object = tile_map.tile_list[tile_number - 1][1]['object']

                    px = (index % layer['width']) * json_map['tilewidth']
                    py = (index // layer['width']) * json_map['tileheight']

                    if tile_object == 'Wall':

                        created_tile = Wall(tile, px, py)
                        self.groups['WALLS'].add(created_tile)

                    elif tile_object == 'Cookie':

                        created_tile = Cookie(tile, px, py)
                        self.groups['COOKIES'].add(created_tile)

                    elif tile_object == 'Fruit':

                        created_tile = Fruit(tile, px, py)
                        self.groups['FRUITS'].add(created_tile)
                        self.groups['ALL'].add(created_tile)

                    object_id = self.get_free_id()
                    object_data = created_tile
                    object_owner = b'Server'

                    self.objects[object_id] = {}
                    self.objects[object_id]['OBJECT_DATA'] = object_data
                    self.objects[object_id]['OBJECT_OWNER'] = object_owner

                    self.groups['ALL'].add(created_tile)


    def load_tile_map(self, map_json_file):
        self.tile_map = TileMap(map_json_file, self.groups)
        self.load_map(self.tile_map)


    def create_object(self, object_id, object_creator, object_group, object_owner):
        '''
        DONE
        Crea un objeto de acuerdo a los datos suministrados, lo ingresa al grupo 
        indicado y le coloca el identificador global del objeto
        '''
        object_data = eval(object_creator)

        self.objects[object_id] = {}
        self.objects[object_id]['OBJECT_DATA'] = object_data
        self.objects[object_id]['OBJECT_CREATOR'] = object_creator
        self.objects[object_id]['OBJECT_GROUP'] = object_group
        self.objects[object_id]['OBJECT_OWNER'] = object_owner

        self.groups[object_group].add(object_data)
        self.groups['ALL'].add(object_data)


    def delete_object(self, object_id):
        '''
        DONE
        Elimina un objeto de todos los grupos en los que esta y lo saca del diccionario
        de objetos
        '''
        object_data = self.objects[object_id]['OBJECT_DATA'].kill()

        self.objects.pop(object_id)


    def update_object(self, object_id, object_parameters):
        '''
        Actualiza un objeto de acuerdo a los parametros ingresados
        '''
        for parameter_name, parameter_data in eval(object_parameters).items():
            self.objects[object_id]['OBJECT_DATA'].__dict__[parameter_name] = parameter_data


    def change_group(self, object_id, new_group):
        '''
        Permite cambiar el grupo de un objeto
        '''
        object_data = self.objects[object_id]['OBJECT_DATA']
        old_group = self.objects[object_id]['OBJECT_GROUP']
        self.groups[old_group].remove(object_data)
        self.objects[object_id]['OBJECT_GROUP'] = new_group   
        self.groups[new_group].add(object_data)


class PacmanClient():

    def __init__(self):
        self.networkingLayer = NetworkingLayer()
        self.gameLayer = GameLayer()


    def setup_identity(self):
        '''
        Configurar identidad(nickname) del jugador
        '''
        done = False
        while not done:

            identity = input(" Enter your nickname! --> ")
            if self.networkingLayer.available_identity(identity):

                self.player_identity = identity
                self.gameLayer.player_identity = identity
                self.networkingLayer.set_identity(s2b(identity))
                #print(self.socket.identity)
                done = True

            else:

                print(" Nickname already taken, try another one")


    def requests(self, waiting = 0):
        '''
        Realiza la operacion solicitada con los datos suministrados, crear, borrar o actualizar
        '''
        while self.networkingLayer.poll(waiting):

            request, *data = self.networkingLayer.fetch_data()

            if request == b'UPDATE_OBJECT':

                object_id = b2i(data[0])
                object_parameters = b2s(data[1])
                self.gameLayer.update_object(object_id, object_parameters)

            elif request == b'DELETE_OBJECT':

                object_id = b2i(data[0])
                self.gameLayer.delete_object(object_id)

            elif request == b'CHANGE_GROUP':

                object_id = b2i(data[0])
                new_group = b2s(data[1])
                self.gameLayer.change_group(object_id, new_group)

            elif request == b'CREATE_OBJECT':

                object_id = b2i(data[0])
                object_creator = b2s(data[1])
                object_group = b2s(data[2])
                object_owner = data[3]
                self.gameLayer.create_object(object_id, object_creator, object_group, object_owner)
                if (object_owner == s2b(self.gameLayer.player_identity)):
                    self.gameLayer.player_id = object_id

            elif request == b'GAME_STATE_PLAYER':
                print(data[0])
                game_state = b2s(data[0])
                self.game_state = game_state

            else:

                print(" Wrong request --> {}".format(request))

            if waiting != 0:
                break


    def run(self):

        pygame.init()

        os.system('clear')
        print("   --- PACMAN CLIENT ---")
        print(" STATUS --> Running...")

        self.setup_identity()

        self.networkingLayer.new_client()
        
        game_settings = self.networkingLayer.game_settings()
        w = game_settings['display_w']
        h = game_settings['display_h']
        tile_map_file = game_settings['map_file']
        player_spawn = game_settings['player_spawn']
        score_start = game_settings['score_start']
        player_id = game_settings['player_id']

        timer_monsters = 0

        pygame.display.set_caption('Pac-Man Client')
        self.gameLayer.display = pygame.display.set_mode([w, h])
        self.gameLayer.clock = pygame.time.Clock()
        self.gameLayer.load_tile_map(tile_map_file)
        self.gameLayer.game_over = False

        self.networkingLayer.current_objects()

        object_creator = "Player('{}', {}, {}, {})".format('Maze/Pac-Man Neon.json', player_spawn[0], player_spawn[1], 50)

        self.networkingLayer.create_object(object_creator, 'PLAYERS')

        player_name = self.gameLayer.player_identity
        color = [255, 255, 255]
        object_creator = "PlayerTitle('{}', 16, {}, '{}', {}, self.objects)".format('Fonts/nevis.ttf', player_id, player_name, color)

        self.networkingLayer.create_object(object_creator, 'TITLES')

        score_pos = score_start
        object_creator = "PlayerScore('{}', 16, {}, {}, '{}', {}, self.objects)".format('Fonts/nevis.ttf', player_id,score_pos ,player_name, color)

        self.networkingLayer.create_object(object_creator, 'ALL')

        self.game_state = 'WAITING'

        waiting_text = Text('Fonts/nevis.ttf', 80, [0, 0, 255], [375, 250], 'WAITING')
        game_over_text = Text('Fonts/nevis.ttf', 80, [255, 0, 0], [375, 250], 'GAME OVER')
        winner_text = Text('Fonts/nevis.ttf', 80, [0, 255, 0], [375, 250], 'WINNER')

        started = False

        self.requests(None)

        while not self.gameLayer.game_over:

            self.requests()

            # Estado de juego donde estoy esperando a que se inicie la partida
            if self.game_state == 'WAITING':

                self.gameLayer.groups['ALL'].add(waiting_text)

            # Estado de juego donde estoy jugando
            elif self.game_state == 'PLAYING':

                if not started:
                    started = True
                    waiting_text.kill()
            
                p = self.gameLayer.objects[player_id]['OBJECT_DATA']

                update_player = {}

                move = [0, 0]

                for event in pygame.event.get():

                    if event.type == pygame.QUIT:
                        self.gameLayer.game_over = True
                        print(" STATUS --> Ended")

                    elif event.type == pygame.KEYDOWN:

                        if event.key == pygame.K_UP:

                            move = [0, -50]
                            update_player['action'] = 'MOVE'
                            update_player['direction'] = 'UP'

                        elif event.key == pygame.K_DOWN:

                            move = [0, 50]
                            update_player['action'] = 'MOVE'
                            update_player['direction'] = 'DOWN'

                        elif event.key == pygame.K_LEFT:

                            move = [-50, 0]
                            update_player['action'] = 'MOVE'
                            update_player['direction'] = 'LEFT'

                        elif event.key == pygame.K_RIGHT:

                            move = [50, 0]
                            update_player['action'] = 'MOVE'
                            update_player['direction'] = 'RIGHT'


                if p.kind == 'PACMAN':

                    # Colisiones entre mi pacman y los enemigos
                    ls4 = pygame.sprite.spritecollide(p, self.gameLayer.groups['ENEMIES'], False, pygame.sprite.collide_mask)
                    for i in ls4:
                        enemy_id = self.gameLayer.find_object_id(i)
                        
                        for title in self.gameLayer.groups['TITLES']:
                            if title.player == i:
                                title_id = self.gameLayer.find_object_id(title)

                        object_owner = self.gameLayer.objects[enemy_id]['OBJECT_OWNER']
                        self.networkingLayer.game_state_player(object_owner, 'LOSE')
                        self.networkingLayer.delete_object(title_id)
                        self.networkingLayer.delete_object(enemy_id)

                        new_score = p.score + 40
                        self.networkingLayer.update_object(player_id, str({'score': new_score}))

                    # Colisiones entre mi pacman y las galletas
                    ls = pygame.sprite.spritecollide(p, self.gameLayer.groups['COOKIES'], False, pygame.sprite.collide_mask)
                    for i in ls:

                        cookie_id = self.gameLayer.find_object_id(i)
                        self.networkingLayer.delete_object(cookie_id)
                        new_score = p.score + 10
                        self.networkingLayer.update_object(player_id, str({'score': new_score}))

                    # Colisiones entre mi pacman y las frutas
                    ls3 = pygame.sprite.spritecollide(p, self.gameLayer.groups['FRUITS'], False, pygame.sprite.collide_mask)
                    for i in ls3:

                        fruit_id = self.gameLayer.find_object_id(i)
                        self.networkingLayer.delete_object(fruit_id)

                        timer_monsters = 8 * 50
                        
                        for p_aux in self.gameLayer.groups['PLAYERS']:
                            p_aux_id = self.gameLayer.find_object_id(p_aux)
                            if p_aux_id != player_id:
                                self.networkingLayer.update_object(p_aux_id, "{'kind': 'MONSTER', 'change_sprite': True}")
                                self.networkingLayer.change_group(p_aux_id, 'ENEMIES')

                    # Manejo del timer cuando alquien convierte a los otros en monstruos
                    if timer_monsters == 1:
                        timer_monsters = 0
                        for p_aux in self.gameLayer.groups['ENEMIES']:
                            p_aux_id = self.gameLayer.find_object_id(p_aux)
                            self.networkingLayer.update_object(p_aux_id, "{'kind': 'PACMAN', 'change_sprite': True}")
                            self.networkingLayer.change_group(p_aux_id, 'PLAYERS')

                    elif timer_monsters > 1:
                        timer_monsters -= 1

                    # Detecto si mi personaje ha ganado
                    if (len(self.gameLayer.groups['COOKIES']) == 0) and (len(self.gameLayer.groups['ENEMIES']) == 0):
                        object_owner = self.gameLayer.objects[player_id]['OBJECT_OWNER']
                        self.networkingLayer.game_state_player(object_owner, 'WIN')



                # Colisiones entre mi personaje y los muros
                p_aux = p
                p_aux.rect.x += move[0]
                p_aux.rect.y += move[1]

                ls2 = pygame.sprite.spritecollide(p_aux, self.gameLayer.groups['WALLS'], False, pygame.sprite.collide_mask)
                if ls2:
                    move = [0, 0]

                # Envio actualizacion de los parametros de mi personaje, si hubo algun cambio
                if update_player != {}:

                    update_player['pos'] = [p.pos[0] + move[0], p.pos[1] + move[1]]
                    update_player['action'] = 'MOVE'
                    update_player['change_sprite'] = True
                    
                    self.networkingLayer.update_object(player_id, str(update_player))

            # Estado de juego donde he perdido
            elif self.game_state == 'LOSE':

                self.gameLayer.groups['ALL'].add(game_over_text)

                for event in pygame.event.get():

                    if event.type == pygame.QUIT:
                        self.gameLayer.game_over = True
                        print(" STATUS --> Ended")

            # Estado de juego donde he ganado
            elif self.game_state == 'WIN':

                self.gameLayer.groups['ALL'].add(winner_text)

                for event in pygame.event.get():

                    if event.type == pygame.QUIT:
                        self.gameLayer.game_over = True
                        print(" STATUS --> Ended")


        
            self.gameLayer.display.fill([0, 0, 0])
            self.gameLayer.groups['ALL'].update()
            self.gameLayer.groups['ALL'].draw(self.gameLayer.display)
            pygame.display.flip()
            self.gameLayer.clock.tick(50)

def main():
    pacmanClient = PacmanClient()
    pacmanClient.run()

if __name__ == "__main__":
    main()