import zmq

class TransportLayer:

    def __init__(self):

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.ROUTER)

        self.identities = []

        self.bind("50000")

    # Coloca a escuchar el socket por el puerto, ip y protocolo dado
    def bind(self, port, ip = "*", protocol = "tcp"):
        self.socket.bind("{}://{}:{}".format(protocol, ip, port))
        print("   --- Started Server ---")

    # Envia un mensaje multiple a un destinatario
    def send(self, destination_ID, message):
        self.socket.send_multipart([destination_ID] + message)

    # Envia un mensaje multiple a una lista de destinatarios
    def multicast(self, message, destinations_ID):
        for ID in destinations_ID:
            self.send(ID, message)

    # Envia un mensaje multiple a todos los destinatarios posibles
    def broadcast(self, message):
        for _ , ID in self.identities.items():
            self.send(ID, message)

    # Recibe un mensaje multiple
    def recv(self):
        return self.socket.recv_multipart()


class GameLayer:

    def __init__(self):
        self.transportLayer = TransportLayer()

    # Escucha todas las solicitudes y las resuelve de acuerdo al caso
    def responses(self):
        while True:

            message = self.transportLayer.recv()
            identity = message[0]
            request = message[1]
            info = message[2:]

            if request == b'CHECK_IDENTITY':

                if not self.find_identity(info[0]):
                    self.transportLayer.send(identity, [b'FREE'])
                else:
                    self.transportLayer.send(identity, [b'TAKEN'])

            elif request == b'NEW_CLIENT':

                self.add_identity(identity)
                self.transportLayer.send(identity, [b'DONE'])
                print(" New client added --> {}".format(identity.decode('ascii')))

            elif request == b'GET_SETTINGS':

                self.transportLayer.send(identity, [b'[1280, 720]'])

            else:

                print(message)
    
    # Busca una identidad en la lista de identidades de la capa de transporte
    def find_identity(self, identity):
        return identity in self.transportLayer.identities

    # Agrega una identidad en la lista de identidades de la capa de transporte
    def add_identity(self, identity):
        self.transportLayer.identities.append(identity)

    # Elimina una identidad en la lista de identidades de la capa de transporte
    def delete_identity(self, identity):
        self.transportLayer.identities.remove(identity)


def main():
    gameLayer = GameLayer()
    gameLayer.responses()

    #transportLayer.send([b"hola1"], ide[0])
    #transportLayer.socket.send_multipart([ide[0], b"hola"])
    #transportLayer.broadcast(['hola'])

if __name__ == "__main__":
    main()