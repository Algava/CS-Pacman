import zmq
import sys
import pygame

class TransportLayer:

    def __init__(self):

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.DEALER)

        self.poller = zmq.Poller()
        self.poller.register(sys.stdin, zmq.POLLIN)
        self.poller.register(self.socket, zmq.POLLIN)

    # Asigna el valor de la identidad
    def set_identity(self, identity):
        self.socket.identity = identity

    # Realiza la conexion con el servidor
    def connect(self, port, ip = "localhost", protocol = "tcp"):
        self.socket.connect("{}://{}:{}".format(protocol, ip, port))

    # Envia un mensaje multiple hacia el servidor
    def send(self, message):
        self.socket.send_multipart(message)

    # Recibe un mensaje multiple
    def recv(self):
        return self.socket.recv_multipart()

    def start(self):
        while True:
            event = dict(self.poller.poll())

            if self.socket in event:
                print(self.recv())

            if sys.stdin.fileno() in event:
                print('?')
                a = input()
                self.send([bytes(a, 'ascii')])

    # Crea socket temporal para verificar existencia de identidad ingresada por el usuario
    def check_identity(self, identity):
        context = zmq.Context()
        socket = self.context.socket(zmq.DEALER)
        socket.connect("tcp://localhost:50000") ## Pendiente al cambio
        socket.send_multipart([b'CHECK_IDENTITY', identity])
        response = socket.recv_multipart()[0]
        socket.close()
        context.term()
        if response == b'FREE':
            return True
        else:
            return False

class GameLayer:

    def __init__(self):
        self.transportLayer = TransportLayer()
        self.setup_nickname()
        self.first_connection()
        self.get_game_settins()

        # Pruebas
        #self.transportLayer.send([b'{1:2}'])

    def setup_nickname(self):
        done = False
        while not done:

            identity = input(" Enter your nickname! --> ")
            identity = identity.encode('ascii')
            if self.transportLayer.check_identity(identity):

                self.player_identity = identity
                self.transportLayer.set_identity(identity)
                done = True

            else:

                print(" Nickname already taken, try another one")

    def first_connection(self):
        self.transportLayer.connect("50000") ## Pendiente al cambio
        self.transportLayer.send([b'NEW_CLIENT', self.player_identity])
        msg = self.transportLayer.recv()
        if msg[0] == b'DONE':
            print(" Welcome {}!".format(self.player_identity.decode('ascii')))

    def get_game_settins(self):
        self.transportLayer.send([b'GET_SETTINGS'])
        msg = self.transportLayer.recv()
        print(msg)
        self.initialize_game(msg[0].decode('ascii'))

    def initialize_game(self, an):
        pygame.init()
        display = pygame.display.set_mode(eval(an))
        end = False
        players = {}
        p = Player()
        players[self.player_identity] = p
        todos = pygame.sprite.Group()
        todos.add(p)
        clock = pygame.time.Clock()
        while not end:

            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    end = True

                elif event.type == pygame.KEYDOWN:

                    if event.key == pygame.K_LEFT:
                        p.rect.x -= 5

                    elif event.key == pygame.K_RIGHT:
                        p.rect.x += 5

                    elif event.key == pygame.K_UP:
                        p.rect.y -= 5

                    elif event.key == pygame.K_DOWN:
                        p.rect.y += 5
            display.fill([0, 0, 0])
            todos.draw(display)
            pygame.display.flip()
            clock.tick(50)

class Player(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([40, 40])
        self.image.fill([0, 255, 127])
        self.rect = self.image.get_rect()


class Pacman:

    def __init__(self):
        pass

    def run(self):
        print("Corriendo")



def main():
    gameLayer = GameLayer()

if __name__ == "__main__":
    main()